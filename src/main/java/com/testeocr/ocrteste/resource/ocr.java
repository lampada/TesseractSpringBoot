package com.testeocr.ocrteste.resource;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import com.testeocr.ocrteste.service.ImageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

@RestController
@RequestMapping("/ocr")
public class ocr{

    @Autowired
    private ImageService imageService;
    
    @GetMapping
    public ResponseEntity<String> olaMundo(){
        
        //BufferedImage bi = ImageIO.read(stream);

        
       // tesseract.doOCR(bi);
        return ResponseEntity.ok("Olá mundo");
    }

    @PostMapping()
    public ResponseEntity<String> traduzir(@RequestParam(name="file") MultipartFile file){
        BufferedImage jpgImage = imageService.getJpgImageFromFile(file);
        Tesseract tesseract = Tesseract.getInstance(); // JNA Interface Mapping
        tesseract.setDatapath("C:\\Program Files (x86)\\Tesseract-OCR");
        String resultado = "";
        try {
            tesseract.setLanguage("por");
			resultado = tesseract.doOCR(jpgImage);
		} catch (TesseractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ResponseEntity.ok(resultado);
    }
}